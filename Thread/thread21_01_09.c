#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;
int **matrice;
int minimo=1000;
int n=0;
int *array=NULL;
int numT=0;
int i =0;

void stampa (int **m,int r){
for (int i=0;i<r;i++){
for (int j=0;j<r;j++){
	printf("%d\t",m[i][j]);
}
	printf("\n");
}
}


void *determina(void *param){
int indice =*(int*)param;


pthread_mutex_lock(&mutex);
for (int i=0;i<n;i++){
array[i]+=matrice[indice][i];
numT--;
}
pthread_mutex_unlock(&mutex);



if (numT<=0){
pthread_mutex_lock(&mutex);
pthread_cond_signal(&condizione);
pthread_mutex_unlock(&mutex);

}
pthread_exit(0);
}


void *minimoFunzione(void *param){
 pthread_mutex_lock(&mutex);
 while (numT>0){
 pthread_cond_wait(&condizione,&mutex);
 }
 pthread_mutex_unlock(&mutex);
 
 for (int i =0;i<n;i++){
  printf("%d\n",array[i]);
 }
 printf("\nil minimo e'");
 for (int i=0;i<n;i++){
 if(array[i]<minimo){
   minimo=array[i];
 }
 }
 printf("%d\n",minimo);
 pthread_exit(0);
}


int main(int argc,char *argv[]){
n=atoi(argv[1]);
numT=n;
array = malloc(n*sizeof(int));
for(int i=0;i<n;i++){
array[i]=0;
}

matrice=(int**)malloc(sizeof(int*)*n);
for(int i=0;i<n;i++){
matrice[i]=(int*)malloc(sizeof(int)*n);
for(int j=0;j<n;j++){
matrice[i][j]=1+rand()%10;
}
}
stampa(matrice,n);
printf("\n");
pthread_t thread[n];
for (int i=0;i<n;i++){
int *indice = malloc(sizeof(int));
*indice=i;
pthread_create(&thread[i],NULL,determina,indice);
}
pthread_create(&thread[n],NULL,minimoFunzione,NULL);

for(int i=0;i<=n;i++){
pthread_join(thread[i],NULL);
}
return 0;
}
