#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <sys/stat.h>
#include <stdio.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;
int n;
int i =0;
int flag=0;
void *scrittore(void *param){

	char *buf = malloc(sizeof(int)); 
	int fd = open("file.txt",O_WRONLY);

	for(i=1;i<=n;i++){
	pthread_mutex_lock(&mutex);
	sprintf(buf, "%d", i);
	write(fd,buf,sizeof(int));
	pthread_mutex_unlock(&mutex);
	usleep(500);
   }
	pthread_exit(0);
}
void *lettore(void *param){

	char *buf = malloc(sizeof(int)); 
	int fd = open("file.txt",O_RDONLY);
	int byteRead=0;
	do{
		pthread_mutex_lock(&mutex);
		byteRead=read(fd,buf,sizeof(int));
		pthread_mutex_unlock(&mutex);
		usleep(500);

		printf("%s\n",buf );
	}while(byteRead>0);

	flag=1;
	pthread_mutex_lock(&mutex);
	pthread_cond_signal(&condizione);
	pthread_mutex_unlock(&mutex);

	pthread_exit(0);
}

void *funzione(void *param){
	pthread_mutex_lock(&mutex);
	while(flag==0){
	 pthread_cond_wait(&condizione,&mutex);
	}
	pthread_mutex_unlock(&mutex);
	sleep(3);
	printf("FIINE\n");
	pthread_exit(0);
}

int main(int argc, char const *argv[])
{
	pthread_t thread[3];
	if (argc<2)		
	{
		perror("Errore parametri");
	}
	n= atoi(argv[1]);
	unlink("file.txt");
	int fd = open("file.txt",O_CREAT,S_IRWXU|S_IRWXG,S_IRWXO);
	close(fd);
	pthread_create(&thread[0],NULL,scrittore,NULL);
	pthread_create(&thread[1],NULL,lettore,NULL);
	pthread_create(&thread[2],NULL,funzione,NULL);

	for (int i = 0; i < 3; i++)
	{
		pthread_join(thread[i],NULL);
	}
	
	return 0;
}