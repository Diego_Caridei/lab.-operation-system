/* Si rializzi un programma in c che con l'ausilio della libreria pthread, lanci n
thread produttori che scrivono concorrentemente numeri random nell'intervallo
1,15 ed un thread consumatore che legge i numeri. Il thread consumatore inizia solo dopo che 
i produttori hanno finito di produrre il numero di elementi da produrre è > 100000 e il 
numero di thread produttori è <= 30 che deve essere specificato da linea di comando 
Usare mutex e var di condizione
*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
int buff[100];
int i=0;
int produrre=100000;
pthread_mutex_t mutex=PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione=PTHREAD_COND_INITIALIZER;

void *produttori(void *arg){
	int indice =*(int*)arg;

	while(i<produrre){
	pthread_mutex_lock(&mutex);
    buff[i]=rand()%15;
    i++;
	pthread_mutex_unlock(&mutex);

	}
	printf("ueue\n");
	// pthread_mutex_lock(&mutex);
	// pthread_cond_signal(&condizione);
	// pthread_mutex_unlock(&mutex);
	pthread_exit(0);
}
void *consuma(void *arg){
	pthread_mutex_lock(&mutex);
	/*i=0;
	while(i < produrre){
		pthread_cond_wait(&condizione,&mutex);
		printf("%d\n",buff[i] );
		i++;
	}*/
	pthread_mutex_unlock(&mutex);
		

	pthread_exit(0);
}

 int main(int argc, char const *argv[])
{
	int n = atoi(argv[1]);
	pthread_t threads[n];
	for (int i = 0; i < n;i++)
	{	int *indice= malloc(sizeof(int));
		*indice=i;
		pthread_create(&threads[i],NULL,produttori,indice);
	}

	pthread_create(&threads[n],NULL,consuma,NULL);
	

	for (i=0;i<=n;i++){
		pthread_join(threads[i],NULL);
	}
	

	return 0;
}