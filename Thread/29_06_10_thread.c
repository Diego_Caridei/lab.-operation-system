#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int m,n,x;
int contatore;
int **matrice;
int *array;
int mThread;

void stampa ( ){
		printf("\n");

for (int i = 0; i < m; i++)
{
	for(int j=0;j<n;j++){
		printf("%d \t",matrice[i][j]);
	}
	printf("\n");
}
}

void *funzione (void *param){
	int indice= *(int*)param;
	for(int j=0;j<n;j++){
		pthread_mutex_lock(&mutex);
		if (x==matrice[indice][j])
		{
			contatore++;
			array[indice]=matrice[indice][j];	
		}
		mThread--;
		pthread_mutex_unlock(&mutex);
	}
	if (mThread==0)
	{
		pthread_mutex_lock(&mutex);
		pthread_cond_signal(&condizione);
		pthread_mutex_unlock(&mutex);

	}
	pthread_exit(0);
}
void *funzione2 (void *param){
	pthread_mutex_lock(&mutex);
	while(mThread>0){
		pthread_cond_wait(&condizione,&mutex);

	}
	pthread_mutex_unlock(&mutex);
	for (int i = 0; i < n; ++i)
	{
		if (array[i]!=0)
		{
			printf("%d\n", array[i]);

		}
	}
	pthread_exit(0);
}

int main(int argc, char const *argv[])
{
	m = atoi(argv[1]);
	n = atoi(argv[2]);
	x = atoi(argv[3]);

	mThread=m;
	pthread_t thread[m];
	array=(int*)malloc(sizeof(int)*n);
	for (int i = 0; i < n; i++)
	{
		array[i]=0;
	}

	matrice=(int**)malloc(sizeof(int*)*m);
	for (int i = 0; i < m; i++)
	{
		matrice[i]=(int*)malloc(sizeof(int)*n);
		for (int j = 0; j < n; j++)
		{
			matrice[i][j]=rand()%10;
		}
	}


	for (int i = 0; i < m; i++)
	{
		int *indice =malloc(sizeof(int));
		*indice=i;
		pthread_create(&thread[i],NULL,funzione,indice);
	}
		pthread_create(&thread[m],NULL,funzione2,NULL);


	for (int i = 0; i <=m ; i++)
	{
		pthread_join(thread[i],NULL);
	}
	 stampa ( );

	return 0;
}