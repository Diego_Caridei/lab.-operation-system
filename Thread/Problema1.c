/*Scrivere un programma che crei 2 thread: leggi e scrivi.
Il thread scrivi ogni secondo stmpa un numero progressivo; il thread leggi
attende la lettura di una stringa da input, ferma il thread scrivi e finisce.
Il thread principale si sincronizza con sul thread leggi. */

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>

pthread_mutex_t mutex =PTHREAD_MUTEX_INITIALIZER;
int glob=0;

void *fucscrivi(){
 while(1){
 pthread_mutex_lock(&mutex);
 glob++;
 printf("Glob vale: %d\n",glob);
 sleep(1);
 pthread_testcanel();
 pthread_mutex_unlock(&mutex); 
 }
 printf("Fine");
 pthread_exit(0);
}


void *funcleggi(void *tScrivi){
 pthread_t scrivi= *((pthread_t*)tScrivi);
 char stringa [512];
 gets(stringa);
 pthread_cancel(scrivi);
}

int main (int argc,char *argv[]){
 pthread_t leggi,scrivi;
 int rtn1,rtn2;
 rtn1 =pthread_create(&scrivi,NULL,&fucscrivi,NULL);
 rtn2 =pthread_create(&leggi,NULL,&funcleggi,scrivi); 
 
 pthread_join(leggi,NULL);
return 0;
}
