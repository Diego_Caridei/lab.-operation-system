/*
Si realizzi un programma C e Posix in ambiente Linux che, 
impiegando la libreria Pthread, generi tre thread. 
I primi due thread sommano 1000 numeri generati 
casualmente ed ogni volta incrementano un contatore. 
Il terzo thread attende che il contatore incrementato 
dai due thread raggiunga un valore 
limite fornito da riga di comando, 
notifica l’avvenuta condizione e termina.
 Utilizzare le variabili condizione.
*/

#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione=PTHREAD_COND_INITIALIZER;
int contatore=0;
int limite=0;

void *somma(void *param){
 int i=0;
 int a,b,c=0;
 while(i<1000 && contatore <=limite){
  pthread_mutex_lock(&mutex);
  a=rand()%10;
  b=rand()%10;
  c=a+b;
  printf("%d\n",c);
  i++;
  contatore++;	
  pthread_mutex_unlock(&mutex);
 }

 if (contatore <=limite){
    printf("Sono qui");
    pthread_mutex_lock(&mutex);
    pthread_cond_signal(&condizione);
    pthread_mutex_unlock(&mutex);	
 } 
 
 pthread_exit(0);
}

void *notifica(void*param){
 
 pthread_mutex_lock(&mutex);
 while(contatore <=limite){
 pthread_cond_wait(&condizione,&mutex);
 }
 pthread_mutex_unlock(&mutex);
 printf("Messaggio arrivato\n" );
 pthread_exit(0);
} 


int main(int argc,char *argv[]){
 pthread_t thread[3];
 if(argc <2){
  printf("Error");
  exit(1);
 }
  limite = atoi(argv[1]);
 int i=0;
 for (i=0;i<3;i++){
    pthread_create(&thread[i],NULL,somma,NULL);
 }
   pthread_create(&thread[3],NULL,notifica,NULL);
 for (i=0;i<=3;i++){
   pthread_join(thread[i],NULL);
 }
 
 return 0;
}
