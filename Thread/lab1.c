 /*Scrivere un programma che acceLa un intero n da riga di comando, 
 crea n thread e poi aspeLa la loro terminazione
 Ciascun thread aspeLa un numero di secondi casuale 
 tra 1 e 10, poi incrementa una variabile
 globale intera ed infine ne stampa il valore
 */
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <unistd.h>
int glob=0;
void *funzione(void *t){
 int pausa = rand()%10;
 sleep(pausa);
 glob+=1;
 printf("Glob vale: %d\n",glob);
 pthread_exit(0);
}

int main(int argc, char *argv[]){
 int N =atoi(argv[1]);
 int i=0;
 int rc;
 pthread_t threads[N];
 for(i=0;i<N;i++){
  rc = pthread_create(&threads[i],NULL,&funzione,NULL);
 }
 
 for (i =0 ;i< N;i++){
   pthread_join(threads[i],NULL);
 }
	
}
