/*Scrivere un programma che preveda un thread al quale passare come parameetro
un array. Il thread, ricevuto l'array ne stampa il contenuto*/

#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
int array[10];
void *print_par(void *par){
 int *p =(int*)par;
 int i=0;
 for(i=0;i<=10;i++){
   printf("%d",*(p+1));
 }
 pthread_exit(0); 
}

int main(int argc, char *argv[]){
int rtn;
pthread_t t1;

int i =0;
for (i=0;i<=10;i++){
   array[i]=rand()%10;
}

int *c=array;
rtn=pthread_create(&t1,NULL,print_par,c);
pthread_join(t1,NULL);

return 0;
}

