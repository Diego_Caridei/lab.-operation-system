/*
Lettore/scrittore
var di condizione
*/
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <sys/stat.h>
#include <unistd.h>
int flag=0;
int n;
int i=13;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;

void *scrittore (void *p){
	int fd = open ("file.txt",O_WRONLY);
	char *buff =malloc(sizeof(int));
	while(i<n){ 
	pthread_mutex_lock(&mutex);
	sprintf(buff, "%d",i);
	write(fd,buff,sizeof(int));
	i++;
	pthread_mutex_unlock(&mutex);
	}

    pthread_exit(0);

}

void *lettore (void *p){
	int fd = open ("file.txt",O_RDONLY);
	char *buff =malloc(sizeof(int));
	int bRead=0;
	do{
	pthread_mutex_lock(&mutex);
	bRead=read(fd,buff,sizeof(int));
	printf("%s\n",buff );
	pthread_mutex_unlock(&mutex);
	sleep(1);
	}while(bRead>0);
	
	if (i>=1)
	{
		pthread_mutex_lock(&mutex);
		pthread_cond_signal(&condizione);
		pthread_mutex_unlock(&mutex);
	}
	
	pthread_exit(0);
}

void *fine(void *p){
	pthread_mutex_lock(&mutex);
	while(i<n){
		pthread_cond_wait(&condizione,&mutex);
	}
	pthread_mutex_unlock(&mutex);
	printf("Finito\n");
	pthread_exit(0);


}
int main(int argc, char const *argv[])
{	
	n=atoi(argv[1]);
	if (n>37 || n<13)
	{
		perror("Valore non corretto");
		exit(1);
	}
	unlink("file.txt");
	int fd = open("file.txt",O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
	close(fd);
	pthread_t thread[3];
	pthread_create(&thread[0],NULL,scrittore,NULL);
	pthread_create(&thread[1],NULL,lettore,NULL);
	pthread_create(&thread[2],NULL,fine,NULL);
	for (int i = 0; i < 3; i++)
	{
		pthread_join(thread[i],NULL);
	}

	return 0;
}