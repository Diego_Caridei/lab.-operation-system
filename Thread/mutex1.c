#include <pthread.h>
#include <stdio.h>

int  x =0; //var glob

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

void *funzione(void *arg){
 int i,j;
 pthread_mutex_lock(&mutex);
 for (i=0;i<11;i++){
  x=i;
  for (j=0;j<20000000;j++){
    printf("x= %d\n", i); 
  } 
 } 
  pthread_mutex_unlock(&mutex);
  pthread_exit(0);
}

int main (int argc ,char * argv[]){
 int res1,res2;
 pthread_t t1,t2;
 res1=pthread_create(&t1,NULL,&funzione,NULL);
 res2=pthread_create(&t2,NULL,&funzione,NULL);
 pthread_join(t1,NULL);
 pthread_join(t2,NULL);
 
 return 0;
}
