/*
Si realizzi un programma che tramite l'ausilio della libreria
pthread lanci n thread con n < 10, tali thread cercano di leggere una variabile
il cui contenuto viene impostato ogni 3 secondi 
generando casulamente un numero intero compreso tra 1 e 100.
Il thread che riesce a leggere la variabile ne scrive il contenuto in una matrice
5x5 allocata dinamicamente, Una volta riempita la matrice, il programma termina facendo 
in modo che ciascun thread stampi in output il propri identificativo seguidal numero 
di volte in cui ha letto la varibile ed uno solo di essi provvede a stampare 
la matrice
*/
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;

int n=0;
int glob=0;
int  i=0;
int j=0;
int **matrice=NULL;
int numT=0;

int flag=0;




void *funzione(void *param){
	int indice =*(int*)param;
	for (int j = 0; j < 5; j++)
	{
		pthread_mutex_lock(&mutex);
		matrice[indice][j]=glob;
		numT --;
		pthread_mutex_unlock(&mutex);

	}
	printf("thread id %d\n",(int)pthread_self());

	if (numT<=0)
	{
		pthread_mutex_lock(&mutex);
		pthread_cond_signal(&condizione);
		pthread_mutex_unlock(&mutex);

	}

	pthread_exit(0);
}
void *funzione2(void *param){
	pthread_mutex_lock(&mutex);
	while(numT>0){
		pthread_cond_wait(&condizione,&mutex);
	}
	pthread_mutex_unlock(&mutex);
	flag=1;

	for (int i = 0; i < 5; i++)
	{
		for (int j=0;j<5;j++){
			printf("%d \t",matrice[i][j] );
		}
		printf("\n");
	}
	pthread_exit(0);
}
void *funzione3(void *param){
	while(flag==0){
	pthread_mutex_lock(&mutex);
		glob=1+rand()%100;
	pthread_mutex_unlock(&mutex);
	sleep(3);
	}

	pthread_exit(0);

}

int main(int argc, char const *argv[])
{
	
	
	if (argc <2)
	{
		perror("Errore argonento");
		exit(0);
	}
	n=atoi(argv[1]);
	pthread_t threads[n];
	numT=n;
	//imposta();
	pthread_create(&threads[n-1],NULL,funzione3,NULL);

	matrice = (int**)malloc(sizeof(int*)*5);
	for(i=0;i<5;i++){
		matrice[i]=(int*)malloc(sizeof(int)*5);
		for(j=0;j<5;j++){
			matrice[i][j]=0;
		}
	}

	for (i=0;i<n-1;i++){
		int *indice=malloc(sizeof(int));
		*indice=i;
		pthread_create(&threads[i],NULL,funzione,indice);
	}
		pthread_create(&threads[n],NULL,funzione2,NULL);

	for (i=0;i<=n;i++){
		pthread_join(threads[i],NULL);
	}

	return 0;
}