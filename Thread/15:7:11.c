/*
Si realizzi un programma c che con l'ausilio della libreria
pthread, lanci n thread che leggano concorrentemente 
da due matirici nxn allocate dinamicamente.Il thread iesimo
calcolerà ogni secondo 
 la somma degli elementi corrispondenti della riga i-esima 
 delle due nratrici.
  Un ulteriore thread attenderà fino a che tutte 
  le entrate della nuova matrice nxn siano calcolate e
   successivamente provvederà a stamparla.
*/

#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
int **m1;
int **m2;
int **m3;
int n;
int numT;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione = PTHREAD_COND_INITIALIZER;

void stampa(int **m){
for (int i=0;i<n;i++){
for (int j=0;j<n;j++){
printf("%d\t",m[i][j]);
}
printf("\n");
}
}


void *funzione1(void *p){
int i = *(int*)p;
sleep(1);

for (int j=0;j<n;j++){
pthread_mutex_lock(&mutex);
m3[i][j]=m1[i][j]+m2[i][j];
numT--;
pthread_mutex_unlock(&mutex);

}
if (numT<=0){
pthread_mutex_lock(&mutex);
pthread_cond_signal(&condizione);
pthread_mutex_unlock(&mutex);
}

pthread_exit(0);
}

void *funzione2(void *p){
pthread_mutex_lock(&mutex);
while(numT>0){
pthread_cond_wait(&condizione,&mutex);
}
pthread_mutex_unlock(&mutex);
stampa(m3);
pthread_exit(0);

}


int main(int argc ,char *argv[]){
if(argc<2){
perror("Errore argomenti");
exit(1);
}
n=atoi(argv[1]);
numT=n;
pthread_t threads[n];

m1=(int**)malloc(sizeof(int*)*n);
m2=(int**)malloc(sizeof(int*)*n);
m3=(int**)malloc(sizeof(int*)*n);
for (int i=0;i<n;i++){
m1[i]=(int*)malloc(sizeof(int)*n);
m2[i]=(int*)malloc(sizeof(int)*n);
m3[i]=(int*)malloc(sizeof(int)*n);
for(int j=0;j<n;j++){
m1[i][j]=1+rand()%10;
m2[i][j]=1+rand()%10;
m3[i][j]=0;
}
}
stampa(m1);
printf("\n");
stampa(m2);
printf("\n");
for (int i=0;i<n;i++){
int *indice =malloc(sizeof(int));
*indice = i;
pthread_create(&threads[i],NULL,funzione1,indice);
}
pthread_create(&threads[n],NULL,funzione2,NULL);

for (int i=0;i<n;i++){
pthread_join(threads[i],NULL);
}
return 0;
}
