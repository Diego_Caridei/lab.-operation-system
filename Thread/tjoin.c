#include <stdlib.h>
#include <stdio.h>
#include <pthread.h>

void *primo(void *parameter){
int s=0,i;
int n = *((int*)parameter);//conversione void a intero
printf("n=%d\n",n);
for (i =1;i<n;i++){
    s+=1;	
 }
 return(void*)s;//riconversione a void
}

int main(int argv,char *argc[]){
 pthread_t t;
 int rtn,somma;
 int arg=5;
 rtn=pthread_create(&t,NULL,&primo,(void*)&arg);
 pthread_join(t,(void*)&somma);
 printf("La somma e' %d\n",somma);
return 0;
}
