/*realizzare un programma in c  che impiegando la libreria Pthread,
crei una matrice di dimensioni nxn con  c dispari, che provveda
a soomare in modo concorrente con quattro thread,gli elementi delle 
due diagionali, della riga centrale e della colonna centrale della matrice 
e ne determini il massimo da assegnare ad un'opportuna variabile */


#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
int **matrice = NULL;
int digprinc=0;
int digsec=0;
int rigcent=0;
int colcent=0;
int n=0;
int i =0;
int j=0;
int max=0;
int array[4];

void *funzione(void *param){
   
   while(i<n){
     pthread_mutex_lock(&mutex);
     digprinc+=matrice[i][i];
     digsec+=matrice[i][n-i-1];
     rigcent+=matrice[n/2][i];
     colcent+=matrice[i][n/2];
     i++;
     pthread_mutex_unlock(&mutex);

   }
   array[0]=digprinc;
array[1]=digsec;
array[2]=rigcent;
array[3]=colcent;
    while (j<4){
    pthread_mutex_lock(&mutex);
 
    if (array[j] > max)
    {
       max  = array[j];
    }
    j++;
    pthread_mutex_unlock(&mutex);

   }
  
  
   pthread_exit(0);
}



void stampa(int **m,int n){
 int i,j=0;
 for (i=0;i<n;i++){
 for(j=0;j<n;j++){
    printf("%d \t",m[i][j]);
  }
   printf("\n");
 }
}


int main (int argc, char *argv[]){
pthread_t threads[4];
int i,j=0;

n=atoi(argv[1]);
//controllo n pari /dispari
if (n%2 == 0){
 perror("N deve essere dispari");
 exit(1);
}
//alloco matrice
matrice = (int**)malloc(sizeof(int*)*n);
for (i=0;i<n;i++){
matrice[i]=(int*)malloc(sizeof(int)*n);
for (j=0;j<n;j++){
 matrice[i][j]=1+rand()%5;
 }

}
	
stampa(matrice,n);
printf("\n");
//Thread
for (i =0 ; i< 4; i++){
 pthread_create(&threads[i],NULL,funzione,NULL);
}
 
for (i =0 ; i< 4; i++){
 pthread_join(threads[i],NULL);
}

printf("la diagonale principale è:%d\n la diag secondaria è:%d\n, la riga centrale è:%d\n la colonna centrale è :%d",digprinc,digsec,rigcent,colcent);
printf("il max è :%d\n",max );
return 0;


}
