/*Scrivere un programma che generi N thread. Ogni thread esegue una stessa 
funzione che stampa la posizione del thread e il valore precedente*/
#include <unistd.h>
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#define N 10
pthread_mutex_t  mutex=PTHREAD_MUTEX_INITIALIZER;

void *stampa(void *param){
int i =*((int*)param);
  sleep(2);
  pthread_mutex_lock(&mutex);
  printf("sono il thread di indice :%d\n",i);
  pthread_mutex_unlock(&mutex);
  
  pthread_exit(0);
}


int main(int argc, char *argv[]){
 pthread_t thread[N];
 int rtn,i=0;
 for (i=0;i<N;i++){
   
   int *indice = malloc(sizeof(int));
   *indice=i; 
   pthread_create(&thread[i],NULL,stampa,indice);
 }
   for (i=0;i<N;i++){
    pthread_join(thread[i],NULL);
   } 
 
 pthread_mutex_destroy(&mutex);
 return 0;
}
