#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t condizione =PTHREAD_COND_INITIALIZER;
int **m1=NULL;
int **m2=NULL;
int **m3=NULL;
int riga;
int riga2;
int colonna; 
int colonna2;
int numThread =0;
void stampa(int**m,int riga,int colonna){
	for (int i = 0; i < riga; ++i)
	{
		for(int j=0;j<colonna;j++){
			printf("%d \t",m[i][j] );
		}
		printf("\n");
	}
}
void *funzione(void *param){
	int i = *(int*)param;

	
		for (int j = 0; j < colonna2; j++)
		{
			for (int k=0;k<colonna;k++){
				usleep(500);
				pthread_mutex_lock(&mutex);
				m3[i][j]=m3[i][j]+(m1[i][k]*m2[k][j]);
				numThread--;
				pthread_mutex_unlock(&mutex);

			}
		}
	pthread_mutex_lock(&mutex);
	if (numThread<=0)
	{
		pthread_cond_signal(&condizione);
	}
	pthread_mutex_unlock(&mutex);

	pthread_exit(0);

}

void *funzione2(void *param){
	pthread_mutex_lock(&mutex);
	while(numThread>0){
		pthread_cond_wait(&condizione,&mutex);
	}
	pthread_mutex_unlock(&mutex);
	stampa(m3,riga,colonna2);
	pthread_exit(0);

}







int main(int argc, char const *argv[])
{

	//mxn nxp
	if (argc<3)
	{
		perror("Errore argomenti");
		exit(1);
	}

	 riga = atoi(argv[1]);
	 colonna = atoi(argv[2]);
	 riga2=atoi(argv[2]);
	 colonna2 = atoi(argv[3]);
	 numThread=riga;

	pthread_t thread[riga+1];	

	if (riga<riga2)
	{
		perror("Errore righe");
		exit(1);
	}

	m1=(int**)malloc(sizeof(int*)*riga);
	for (int i = 0; i < riga;i++)
	{
		m1[i]=(int*)malloc(sizeof(int)*colonna);
		for (int j = 0; j < colonna; j++)
			{
				m1[i][j]=1+rand()%5;
			}	
	}

	m2=(int**)malloc(sizeof(int*)*riga2);
	for (int i = 0; i < riga2;i++)
	{
		m2[i]=(int*)malloc(sizeof(int)*colonna2);
		for (int j = 0; j < colonna2; j++)
			{
				m2[i][j]=1+rand()%5;
			}	
	}

	m3=(int**)malloc(sizeof(int*)*riga);
	for (int i = 0; i < riga;i++)
	{
		m3[i]=(int*)malloc(sizeof(int)*colonna2);
		for (int j = 0; j < colonna2;j++)
			{
				m3[i][j]=0;
			}	
	}

	//thread
	for (int i = 0; i < riga; i++)
	{
		int *indice=malloc(sizeof(int));
		*indice=i;
		pthread_create(&thread[i],NULL,funzione,indice);
	}
	pthread_create(&thread[riga],NULL,funzione2,NULL);

    for (int i = 0; i <= riga; i++)
	{
		pthread_join(thread[i],NULL);
	}

	return 0;
}