/*
Si realizzi un programma C “fork1.c” che stampi a video il pid del
processo che lancerà un fork e dopo l’esecuzione (fork), sia in grado di
identificare i processi genitore e figlio con i relativi pid testando i
valori di ritorno da fork.*/


#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc,char * argv[]){


	pid_t pid;
	int numeroRicevuto = atoi(argv[1]);
	pid=fork();
	if (pid==0)
	{
		int somma = 15 + numeroRicevuto;
		printf("Sono il figlio %d\n",somma );
		exit(0);
	}
	else if (pid>0){
		int somma = 10 + numeroRicevuto;
		printf("Sono il padre %d\n",somma );
	}
	return 0;
}