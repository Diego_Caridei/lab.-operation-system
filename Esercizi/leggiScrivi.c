#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <string.h> 
int bRead;

int main(int argc,char *argv[]){
	if(argc < 2){
		return 0;
	}else{
		unlink(argv[1]);
		int fp = open(argv[1],O_CREAT|O_TRUNC|O_RDWR,777);
		pid_t PF ;
		PF =fork();
		if(PF == 0){
			char *string=malloc(100*sizeof(char));
			printf("Inserisci una frase\n");
			scanf ("%s",string);
			write (fp,string,strlen(string));
			exit(0);
		}
		else if(PF>0){
			waitpid(PF,NULL,0);
			printf("Sono padre\n");
			lseek(fp,0,SEEK_SET);
			char *buf=malloc(sizeof(char));
			do{
				bRead = read(fp,buf,sizeof(char));
				printf("%s",buf );

			}
			while(bRead >0); 
			close(fp);
			exit(0);
		}


	}

	return 0;
}