#include <stdio.h>
#include <unistd.h>

int main (int argc,char * argv[]){
	pid_t pid;
	pid = fork();
	printf("Primo pid %d\n",pid );
	if (pid==0)
	{
		printf("Sono il figlio pid:%d\n",getpid() );
	}
	else{
		printf("Sono il padre pid:%d\n",getpid() );

	}
	return 0;
}