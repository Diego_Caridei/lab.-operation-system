/*
 •  Scrivere un programma C in cui un processo crea un processo figlio
 •  Il processo figlio calcola la sequenza di Fibonacci di ordine n (n<=12). Quando termina res/tuisce il valore calcolato come codice di terminazione
 •  Il padre aJende la terminazione del figlio ed esamina lo stato di terminazione
 •  Se lo stato di terminazione è rela/vo ad una terminazione con successo e il codice di terminazione è un valore minore di 50
 •  Crea un secondo figlio che esegue il comando ls –al a.out
 •  AJende il secondo figlio, stampa un messaggio e termina
 •  Altrimen/, stampa un messaggio e termina
 */

#include <stdio.h>
#include <unistd.h>
int Fibonacci(int n)
{
   if ( n == 0 )
      return 0;
   else if ( n == 1 )
      return 1;
   else
      return ( Fibonacci(n-1) + Fibonacci(n-2) );
} 

int main(int argc, const char * argv[]) {
    pid_t pid;
    pid=fork();
    if (pid==0) {
        int  fib =Fibonacci(1);
        exit(fib);
    }
    else if (pid>0){
        int stato;
        waitpid(pid,&stato,0);


        if (WIFEXITED(stato)>0 && WIFEXITED(stato)<=50)
        {
            pid=fork();
            if (pid ==0){
            execlp("ls", "ls","-al","a.out",NULL);
            exit(0);
        }
            waitpid(pid, NULL,0);
            printf("Fine\n");
        }

        else{
            printf("Frateeeee\n");
            exit(0);
        }
    }
    return 0;
}