#include <stdio.h>
#include <unistd.h>
#include <sys/types.h> /*open, lseek */
#include <sys/stat.h> /*open */
#include <fcntl.h> /*open*/ 
#define BUFDIM 1000
int main(int argc,char *argv[]){
	int fd,nread;
	char buffer[ BUFDIM ]; 
	fd = open("alfabeto.txt",O_RDONLY| O_CREAT, S_IRUSR | S_IWUSR);
	while( (nread=read(fd, buffer, BUFDIM) ) > 0 ){
		printf("%s\n",buffer );
	}
	return 0;
}