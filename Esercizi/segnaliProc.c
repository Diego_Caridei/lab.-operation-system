#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <math.h>
int glob=0;
pid_t A = 0;
pid_t B = 0;

void figlioB(){
	int reciproco = -glob;
	sleep(glob*2);
	kill(A,SIGUSR1);
	exit(0);
}


void segnale( int segn){
	printf("Sono segnale\n");
	if(segn==SIGUSR1) {

		printf("Yes %f\n",pow(glob,3));
	}else if(segn==SIGUSR2){
		figlioB();

	}
}


int main(int argc,char *argv[]){
	if (argc <2)
	{
		return 0;
	}
	else{
		glob = atoi(argv[1]);
		
		A = fork();

		if (A == 0){

			signal(SIGUSR1,segnale);
			while(1) {
			    /* code */
			}
			printf("Sono A\n");

			pause();
			printf("FINE A\n");

		}
		else{
			B =fork();
			if(B==0){
			//Sono B
				signal(SIGUSR2,segnale);
				printf("Sono B\n");

				while(1) {}


			}
		if(glob%2==0){
			printf("Sono pari\n" );
			kill (A,SIGUSR1);

		}else{
			printf("Sono dispari\n" );

			kill (B,SIGUSR2);

		}
		waitpid(A,NULL,0);
		waitpid(B,NULL,0);

	}


}

return 0;
}