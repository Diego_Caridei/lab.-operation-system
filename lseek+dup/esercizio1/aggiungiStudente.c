/*
Si realizzi un programma C “aggiungiStudente.c” che scrive
il cognome dello studente passato da tastiera in coda al file
elencoStudenti.txt. (Ogni linea è lunga 41 byte: 40 per il cognome
1 byte per il byte di newline)
Struttura file elencoStudenti.txt:
Bianchi
Menna
Rossi
Corcione
Capasso
*/
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#define lunghezza 41
int main(int argc, char const *argv[])
{
	int fd;
	char cognome [lunghezza];
	printf("Inserisci il nome\n");
	scanf("%s",cognome);
	cognome[strlen(cognome)] = '\0'; 


	if ((fd=open("elencoStudenti.txt",O_RDWR))  <0)
	{
		printf("Error open\n" );
	}else{

		if (lseek(fd,0L,SEEK_END)<0)
		{
			printf("Error lseek\n" );
		}
		else{
			write(fd,cognome,strlen(cognome));
		}

	}
	close(fd);


	return 0;
}