#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
	int fd;
	if((fd = open("dup.txt",O_RDWR|O_CREAT, S_IRUSR | S_IWUSR))<0){
		perror("Errore");
	}
	//Chiudo lo standard output
	close(1);
	dup(fd);
	close(fd);
	printf("ciao mondo\n");
	return 0;
}