#include <stdio.h>
#include <signal.h>
#include <stdlib.h>

void intHandler(); //gestore segnale interrupt
void quitHandler(); //gestore segnale quit

int main(int argc, char const *argv[])
{
	signal(SIGINT,intHandler);
	signal(SIGQUIT,quitHandler);
	printf("Ctrl-C disabilitato Usare Ctrl-\\ per uscire\n");	
	while(1);
	return 0;
}

void intHandler(){
	printf("Interrupt Ctrl-C disabilitato\n");
	signal(SIGINT,intHandler);//Registra di nuovo l handler

}
void quitHandler(){
	printf("Ctrl-\\ Uscita\n");
	exit(0);
}