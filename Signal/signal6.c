/*Scrivere un programma che in esecuzione crei due processi figli.
il padre  registra tramite signal il segnale SIGINT LA CUI RICEZIONE DA PARTE DI CIASCUN FIGLIO COMPORTI CHE QUESTO:
1)SI IDENTIFICHI MEDIANTE LA STAMPA DEL PROPRIO PID;
2)STAMPI IL NUMERO DI SEGNALE RICEVUTO .
SUCCESSIVAMENTE IL PADRE LANCIA DUE WAIT UNA PER CIASCUN FIGLIO, a seguito delle quali occorre stampare le ripesttive:
WIFEXITED()
WEXITSTATUS()*/
#include <stdio.h> 
#include <signal.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/wait.h>

void handler(){
	printf("Pid %d parent %d\n",getpid(),getppid() );
	exit(0);
}
int main(int argc, char const *argv[])
{
	pid_t pd1,pd2;
	int ret;
	int status;
	signal(SIGINT,handler);
	if((pd1=fork())==0){
		while(1);
		exit(0);
	}
	if((pd2=fork())==0){
		while(1);
		exit(0);

	}
	waitpid(pd1,NULL,0);
	waitpid(pd2,NULL,0);
	printf("WIFEXITED %d\n", WIFEXITED( status ));
	printf("WEXITSTATUS %d\n", WEXITSTATUS( status ) );

	while(1);

	

	return 0;
	
}