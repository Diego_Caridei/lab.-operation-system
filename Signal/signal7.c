/*
Scrivere un programma che crei un sottoprocesso che avanza alternativamente con il padre, con pause
di un secondo.Ogni volta che uno dei due processi riceve un segnale de identificarsi, stampare 
il segnale ricevutoed il numero di volte che ciò è accaduto
*/
#include <stdio.h> 
#include <signal.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/wait.h>
void handler(){
	printf("Pid:%d\n",getpid());
}
int main(int argc, char const *argv[])
{
	pid_t pid;
	signal(SIGUSR1,handler);
	if ((pid=fork())==0)
	{
		
		printf("Figlio\n");
		sleep(1);
		kill(getppid(),SIGUSR1);
		pause();

	}
		printf("Padre\n");
q		kill(pid,SIGUSR1);
		pause();

		while(1);



	return 0;
}