/*Scrivere un programma che metta a disposizione un signal 
handler che abbia il compito di consentire l'uscita dal programma
con richiesta di conferma mediante  interrupt Ctrl-C
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
void intHandler(){
	int scelta;
	printf("Vuoi uscire dal programma?\npremi 1 \n");
	scanf("%d",&scelta);
	if (scelta==1)
	{
		exit(1);
	}
}

int main(int argc, char const *argv[])
{
	signal(SIGINT,intHandler);
	while(1);	
	return 0;
}