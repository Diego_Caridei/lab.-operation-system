#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

void my_handler(int signum)
{
    if (signum == SIGUSR1)
    {
        printf("Received SIGUSR1!\n");
        sleep(4);
        exit(0);
    }
     if (signum == SIGINT)
    {
        printf("Received SIGINT!\n");

        exit(0);
    }

}
int main(int argc, char const *argv[])
{
	signal(SIGUSR1, my_handler);
	signal(SIGINT, my_handler);

	kill(getpid(), SIGUSR1);

	while(1);
	return 0;
}