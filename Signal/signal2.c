/* Scrivere un programma che consenta di disabilitare il Ctrl-C e di 
contare il numero di volte che questa combinazione di caratteri viene premuta 
il programma termina con Ctrl-\
*/
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
int contatore=0;
void intHandlar();
void quitHandlar();
int main(int argc, char const *argv[])
{
	signal(SIGINT,intHandlar);
	signal(SIGQUIT,quitHandlar);
	printf("Ctrl-C disabilitato premere Ctrl-\\ per uscire\n");
	while(1);

	return 0;
}

void intHandlar(){
	contatore+=1;
	printf("Contatore vale:%d\n",contatore );
	signal(SIGINT,intHandlar);

}
void quitHandlar(){
	printf("Esco\n");
	exit(0);
}