/*Scrivere un programma che crei due processi per la stampa dei numeri pari
e l'altro per i numeri dispari,tra 1 e 10.Ciascuno dei due processi è gestito
da SIGUSR1 e SIGUSR2 inoltre bisogna prevedere  un terzo handler per 
gestire la terminazione del padre.
Il processo viene lanciato in background e l'esecuzione dei due handler
viene ottenuta mediante il comando kill da shell 
ESEMPIO kill -s SIGUSR1 PID
*/
#include <stdio.h> 
#include <signal.h> 
#include <stdlib.h> 
#include <unistd.h>
#include <sys/wait.h>

void handler(int segnale){

	if (segnale==SIGUSR1)
	{
		printf("Stampo i numeri pari\n" );
		for (int i = 1; i <= 10; ++i)
		{
			if (i%2==0)
			{
				printf("%d\n",i );
			}
		}

	}
	else if (segnale==SIGUSR2)
	{
		printf("Stampo i numeri dispari\n" );
		for (int i = 1; i <= 10; ++i)
		{
			if (i%2==1)
			{
				printf("%d\n",i );
			}
		}

	}
	else if (segnale==SIGINT)
	{
		printf("Ciao ciao\n");
		exit(0);
	}

}
int main(int argc, char const *argv[])
{
	signal(SIGUSR1,handler);
	signal(SIGUSR2,handler);
	signal(SIGINT,handler);
	printf("%d\n",getpid() );

	while(1);

	return 0;
}