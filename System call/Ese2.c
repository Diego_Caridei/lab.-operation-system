/* Scrivere un programma che carichi in un file una serie di stringhe
il file viene gestito con le primitive a basso livello e termina quando 
si immette la stringa FINE */
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#define BUFF 100

int main(int argc, char const *argv[])
 {
 	char buf[BUFF];
 	int fd;
 	int i=0;
 	unlink("fiel2.txt");
 	if((fd=open("fiel2.txt",O_WRONLY|O_CREAT,777) )<0){
 		printf("Errore nell'apertura del file\n");
 		exit(0);
 	}else
 		for(i=0;i<=BUFF;i++){
 			buf[i]=0;
 		}

 		//Finchè non contiene FINE continuo a scrivere
  		while(strcmp(buf,"FINE")){
 		//Inizializzo il buffer
  		printf("Inserisci la stringa da scrifere nel file oppure FINE \n");
  		scanf("%s",buf);
 		//printf("%s\n",buf);
 		for(i=0;i<=BUFF;i++){
 			write(fd,&buf[i],sizeof(char));
 		}
 		

 	}


 	
 	return 0;
 } 