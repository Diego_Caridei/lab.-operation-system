/*
Scrivere un programma che carichi un file ,gestito a basso livello
una serie di numeri interi contenuti in un array
*/
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#define  BUFF 10
int main(int argc, char const *argv[])
{
	char *buf2= malloc(sizeof(char));
	int buf[BUFF]={1,2,3,4,5,6,7,8,9,10};
	int i=0;
	int fd;
	if ((fd=open("file.txt",O_WRONLY|O_CREAT))<0)
	{
		printf("Errore\n");
		exit(1);
	}

	else{
		while(i<BUFF){
			char p[BUFF];
			sprintf(p,"%d",buf[i]);
			write(fd,p,sizeof(char));
			i++;
			close(fd);
		}
		//Lettura e stampa
	}
	if ((fd=open("file.txt",O_RDONLY))<0)
	{
		printf("Errore\n");
		exit(1);

	}
	
	do
	{
		printf("%s\n",buf2 );
	} while (read(fd,buf2,sizeof(char)));
	close(fd);
		
	return 0;
	
}