#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	pid_t pid;
	int fd[2];
	char buf[256];
	pipe(fd);
	if ((pid=fork())==0)
	{	close(fd[1]);//Chiudo scrittura
		read(fd[0], buf,19 );
		printf("%s\n",buf);
		close(fd[0]);
		exit(0);
	}
	close(fd[0]);//chiudo lettura
	char buf2[256];
	sprintf(buf2, "%s","Luke sono tuo padre");

	 write( fd[1], buf2, sizeof(buf2));
	 close(fd[1]);

	return 0;
}