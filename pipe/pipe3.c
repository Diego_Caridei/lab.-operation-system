/*
Figlio scrive padre legge
*/
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	int fd[2];
	pid_t pid;
	pipe(fd);
	pid=fork();
	if(pid==0){
		char msg[256];
		sprintf(msg, "%s\n", "Dart fener sono tuo Figlio");
		close(fd[0]);//Chiudo lettura e scrivo
		write(fd[1],msg,sizeof(msg));
		close(fd[1]);
		exit(0);
	}
	close(fd[1]);
	char b[256];
	read(fd[0],b,26);
	printf("%s\n",b );
	close(fd[0]);

	return 0;
}