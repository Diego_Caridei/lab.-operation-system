#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/wait.h>

pid_t pid1,pid2,pid3;
int pipe1[2],pipe2[2];

void exec1(){
dup2(pipe1[1],1);
close(pipe1[0]);
close(pipe1[1]);
execlp("cat","cat","file.c",NULL);
perror("Exec1");
_Exit(1);
}
void exec2(){
dup2(pipe1[0],0);
dup2(pipe2[1],1);
close(pipe1[0]);
close(pipe1[1]);
close(pipe2[0]);
close(pipe2[1]);
execlp("grep","grep","printf",NULL);
perror("Exec2");
_Exit(1);
}
void exec3(){
dup2(pipe2[0],0);
close(pipe2[0]);
close(pipe2[1]);
execlp("wc","wc","-c",NULL);
perror("Exec 3");
_Exit(1);
}

int main(int argc, char *argv[]){
pipe(pipe1);
pid1=fork();
if(pid1==0){
 exec1();
}
pipe(pipe2);
pid2=fork();
if(pid2==0){
 exec2();
}
close(pipe1[0]);
close(pipe1[1]);
pid3=fork();
if(pid3==0){
 exec3();
}
close(pipe2[0]);
close(pipe2[1]);
waitpid(pid1,NULL,0);
waitpid(pid2,NULL,0);
return 0;
}
