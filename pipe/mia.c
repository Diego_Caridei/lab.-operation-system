#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <stdlib.h>
#include <math.h>
int main(int argc,char *argv[]){
  pid_t pid;
  int pip [2];
  pipe(pip);
  pid=fork();
  if(pid==0){
    close(pip[1]);
    char buf[10];
    read(pip[0],buf,sizeof(int));
    int num =atoi(buf);
    if(num%2==0){
     printf("%f",pow(num,2));
    }else{
         printf("%d",num*2);
     }
    exit(0);		
  }
  //Sono il padre
  int num =6;
  close(pip[0]);
  char *bnum=malloc(1*sizeof(int));
  sprintf(bnum,"%d",num);
  write(pip[1],bnum,sizeof(bnum));
  waitpid(pid,NULL,0);
return 0;
}
