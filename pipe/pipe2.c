/* Padre scrive figlio legge*/
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#define SIZE 1024
int main(int argc, char const *argv[])
{
	int pfd[2];
	int nread;
	int pid;
	char buf[SIZE];
	if (pipe(pfd)==-1)	
	{	
		perror("pippe() fallita");
		exit(-1);
	}
	if ((pid=fork())<0)	
	{	
		perror("fork() fallita");
		exit(-2);
	}



	//Figlio
	if (pid==0)
	{
		close(pfd[1]);
		while((nread=read(pfd[0],buf,SIZE)!=0)){
			printf("Il figlio legge %s\n",buf );
		}
		close(pfd[0]);
	}
	//Padre
	else{
		close(pfd[0]);
		strcpy(buf,"Sono tuo padre");
		write(pfd[1],buf,strlen(buf)+1);
		close(pfd[1]);
	}
	exit(0);

	return 0;
}