//Pipe interna ad un processo
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>

#define MESSAGE_LENGHT 72
void cut_tail_blank(char *s);
void add_tail_blank(char *s);
void scrivi_messaggi(int pipe_fd[]);//Scrive nelle pipe
void leggi_messaggi(int pipe_fd[]);//legge nelle pipe

int main(int argc, char const *argv[])
{
	int status,pipe_fd[2];//dichiaro la pipe
	status=pipe(pipe_fd);//la creo
	if (status==-1)
	{
		printf("Errore nella pipe\n");
		exit(1);
	}
	scrivi_messaggi(pipe_fd);//deposito il messaggio nella pipe
	leggi_messaggi(pipe_fd);//preleva il messaggio dalla pipe
	close(pipe_fd[0]);//chiudo la pipe in lettura e
	close(pipe_fd[1]);//chiudo la pipe in scrittura
	return 0;
}
void scrivi_messaggi(int pipe_fd[]){
	int i;
	char messaggio[256];
	for (int i = 0; i < 3; ++i)
	{
		printf("messaggio[%d]=\n",i );
		scanf("%s",messaggio);
		add_tail_blank(messaggio);
		if (write(pipe_fd[1],messaggio,MESSAGE_LENGHT)!=MESSAGE_LENGHT)
		{	
			printf("errore nella write\n");
			exit(1);
		}
		return;
	}
}
void leggi_messaggi(int pipe_fd[]){
	int i;
	char messaggio[256];
	for (int i = 0; i < 3; ++i)
	{
	   if (read(pipe_fd[0],messaggio,MESSAGE_LENGHT)<=0){
	   		printf("errore nella read\n");
			exit(1);
	   }
	   cut_tail_blank(messaggio);
	   printf("messaggio[%d]=%s\n",i,messaggio );

	}
	return;
}
void cut_tail_blank(char *s){
	int i;
	for (i=MESSAGE_LENGHT-1;i>=0;i--)
	{
		if (s[i]!=' ')
		{
			s[i+1]='\0';
			return;
		}
	}
	s[i]='\0';
	return;
}
void add_tail_blank(char *s){
	while(strlen(s)<MESSAGE_LENGHT){
		strcat(s," ");
		s[MESSAGE_LENGHT]='\0';
		return;
	}
}

