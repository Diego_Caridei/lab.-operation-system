#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	pid_t pid;
	int stato;
	int pipe1 [2];
	pipe(pipe1);
	pid=fork();
	if (pid==0)
	{
		dup2(pipe1[0],0);//dublico quello che mi serve
		close(pipe1[0]);
		execlp("cat","cat","history.c",NULL);
		exit(0);
	}
	waitpid(pid,NULL,0);
	char b[900000];
	close(pipe1[1]);
	read(0,b,sizeof(b));
	printf("%s\n",b );
	return 0;
}