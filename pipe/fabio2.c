/*
Processi: un padre 2 figli:
Il padre genera un numero intero tramite la rand e la passa ai figli tramite una pipe
i figli accedono concorrentemente ai valori se pari scrive il numero su un file pari.txt
se è dispari su un file dispari.txt
*/
#include <unistd.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>

int main(int argc, char const *argv[])
{
	pid_t figlioPari,figlioDispari;
	int fd[2];
	pipe(fd);
	if ((figlioPari=fork())==0)
	{
		printf("figlioPari Pid %d  \n",getpid() );
		close(fd[1]);
		char *buf=malloc(1*sizeof(int));
		read(fd[0],buf,sizeof(int));
		int numero =atoi(buf);
		unlink("pari")

		if(numero%2==0){
		}
		exit(0);
	}
	if ((figlioDispari=fork())==0){

		printf("figlioDispari Pid %d  \n",getpid() );
		exit(0);
	}
	int i =0;
	for (int i = 0; i < 50; ++i)
	{
		int f=rand()%10;
		close(fd[0]);//Chiudo in lettura e scrivo
		char *buf =malloc(1*sizeof(int)); 
		sprintf(buf,"%d",f);
		write(fd[1],buf,sizeof(buf));
		close(fd[1]);

	}
	waitpid(figlioPari,NULL,0);
	waitpid(figlioDispari,NULL,0);
	printf("Pid %d  \n",getpid() );




	return 0;
}