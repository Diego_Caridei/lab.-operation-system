/*
Figlio esegue ls e l'output lo stampa il padre
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	pid_t pid;
	int fd[2];
	pipe(fd);
	char p[900];
	pid=fork();
	if (pid==0){
		dup2(fd[0],0);//Duplico il descrittore di lettura sullo standard read
		close(fd[0]);
		execlp("ls","ls",NULL);
		exit(0);
	}	
	waitpid(pid,NULL,0);
	close(fd[1]);//Chiudo la scrittura
	read(0,p,sizeof(p));
	printf("%s\n",p );
	return 0;
}