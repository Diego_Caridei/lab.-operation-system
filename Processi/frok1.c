/* Scrivere un programma che  riceve da linea di comandoo 2 interi.
Il programma crea un sottoprocesso che ne calcola il prodotto 
mentre il processo padre ne calcola la somma.
*/
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{

	int n1,n2=0;
	if (argc<3){
		exit(-1);
	}
	n1=atoi(argv[1]);
	n2=atoi(argv[2]);
	pid_t f1;
	if((f1=fork())==0){
		printf("Sono il figlio con il pid: %d e calcola il prodotto:%d\n",getpid(),n1*n2 );
		exit(0);
	}
	else{
		printf("Sono il padre con il pid: %d  e calcola la somma:%d\n",getpid(),n1+n2 );
		exit(0);

	}
	return 0;
}