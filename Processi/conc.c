/*
Scrivere un programma concorrente che riceva da linea di comando 
2 parametri, il primo indica il nome di un file F e il secondo 
un numero intero N. Il Programma deve creare due processi
che scrivono N volte su F un dato messaggio che consenta di
distinguere  il processo che l' ha scritto.
*/
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	//Controllo gli argomenti
	if (argc <3)
	{
		return 0;
	}
	int N = atoi(argv[1]);
	int fd;
	int i;
	unlink(argv[2]);
	fd=open(argv[2],O_WRONLY|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);
	char buf1[]="Testo uno per il pid1\n";
	char buf2[]="Testo due per il pid2\n";	
	

	pid_t pid1, pid2;
	pid1 =fork();

	//Sono il padre
	if (pid1>0)
	{
		pid2=fork();
		//
		if (pid2==0)
		{
			for(i =0;i<N;i++){
				write(fd,buf1,strlen(buf1));
				
			}
			exit(0);
		}

		waitpid(pid1, NULL, 0);
		waitpid(pid2, NULL, 0);
		close(fd);
	}
	
	//Figlio
	else if (pid1==0){
		for(i =0;i<N;i++){
				write(fd,buf2,strlen(buf2));
				
			}
			exit(0);
	}


	return 0;
}