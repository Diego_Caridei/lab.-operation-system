/* Scrivere un programma che generi un processo figlio che termini prima del figlio
trasformandolo in un processo orfano.
Il processo padre  lancia una fork stampa i suoi dati e termina immediatamnete.
il figlio ritarda 5 sec e al termine si accorge della scomparsa del processo padre 
diventando quindi orfano. Il processo init lo adotta ed assume quindi pid uguale ad 1
*/
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
int main(int argc, char const *argv[])
{
	pid_t pid;
	pid = fork();

	if(pid != 0){//Sono il padre
		printf("Sono il padre con pid: %d\n",getpid() );
		exit(1);
	}
	if(pid==0){//Sono il figlio
		sleep(5);
		printf("\n");
		if( getppid()==1){//poichè il processo initi ha pid = 1
			printf("Sono orfano ho pid %d e parent pid :%d\n ",getpid(),getppid());
			exit(0);
		}
	}


	return 0;
}