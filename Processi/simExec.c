/*
Simulare tramite la exec() una esecuzione di un comando di shell.
1)il padre instanzia una fork che duplica la shell
2)il padre viene  messo in attesa della terminazione del figlio
3)il figlio chiama l exec() per eseguire il programma voluto
4)Il programma si sostituisce al figlio ed esegue
5) alla fine dell'esecuzione avvisa il padre la shell sostesa che riprenderà il controllo
Tenendo presente la sintassi della execl() si può pensare di leggere la stringa command 
ed una stringa option che rappresentano i parametri */

#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
int main(int argc,char *argv[]){
	pid_t PF;
	char *comando= malloc(10*sizeof(char));
	char *path=malloc(10*sizeof(char));
	char *opzioni=malloc(10*sizeof(char));

	printf("Insrisci il comando\n");
	scanf("%s",comando);
	printf("Insrisci il path\n");
	scanf("%s",opzioni);
	
	PF=fork();

	//Sono il figlio
	if (PF==0)
	{

		printf("sono il figlio\n");
		execlp(comando ,opzioni,  NULL );
		exit(0);

	}
	else{
		 printf("Sono il padre attendo\n");
		 waitpid(PF, NULL,0);
		 printf("Fine attesa\n");

	}

	return 0;
}