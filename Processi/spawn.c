#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
int spawn(char *program,char *arg_list[]);
int main(int argc, char const *argv[])
{
	char *arg []={"ls","-l","/",NULL};
	spawn("ls",arg);
	return 0;
}

int spawn(char *program,char *arg_list[]){
	pid_t child_pid;
	child_pid=fork();
	if (child_pid!=0){
			return child_pid;
	}
	else{
		if(execvp(program,arg_list)==-1){
			printf("Error execvp()\n");
			exit(1);
		}
	}	
	return 0;
}