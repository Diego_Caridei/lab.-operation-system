#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
	pid_t pid1,pid2,pid3;
	
	printf("Esempi S.C. exec:\n");
	//Path comando argomenti ritorna -1 errore
	if((pid1=fork())==0){
		printf("Comando execl()\n");
		execl("/bin/ls","ls","-l",(char *)0);
		exit(0);
	}
	if((pid2=fork())==0){
		sleep(1);
		printf("Comando execv()\n");
		char *arg[]={"ls","-l",NULL};
		//Path, vettore
		execv("/bin/ls", arg);
		exit(0);

	}
	else{
		//sono il padre
		  waitpid(pid1, NULL, 0);
		  waitpid(pid2, NULL, 0);
		  sleep(1);
		  printf("Comando execlp()\n");
		  //Comando,argmenti
	      execlp("ls","-l","-a",NULL);

	}


	





	return 0;
}