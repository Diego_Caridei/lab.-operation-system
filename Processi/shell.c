/*
Simulazione di una shell
*/
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>

int main(int argc, char const *argv[])
{
	char comando[256];
	char opzione[256];
	pid_t pid;

	while(1){
		printf("Inserisci comando oppure logut per terminare\n");
		scanf("%s",comando);
		//Controllo l'uscita
   		if (strcmp(comando,"logut") == 0){
			printf("Esco\n");
			exit(0);
		}
		printf("Inserisci l'opzione\n");
		scanf("%s",opzione);
		pid=fork();
		//Sono il figlio
		if(pid==0){
			int status = execlp(comando,comando, opzione,NULL);
			exit(status);
		}
		else if(pid>0){
		waitpid( pid, NULL, 0);

		}
	
	}
	
	return 0;
}