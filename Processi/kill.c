#include <signal.h>
#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
void padre();//Funzione che esegue il padre
void figlio(); //Funzione eseguita dal figlio
pid_t pid;
int main(int argc, char const *argv[])
{
	pid=fork();
	if(pid>0){
		//Sono il padre

		padre();
	}else {
		figlio();
		exit(0);
	}
	return 0;
}
void padre(){
	int retcode;
	printf("PID processo figlio =%d\n",(int)pid);
	printf("Il processo padre inizia un attesa di 10 sec \n");
	sleep(10);
	printf("Il processo padre elimina il processo figlio\n");
	retcode=kill(pid,SIGKILL);
	printf("Processo figlio terminato" );
}

void figlio(){
	printf("Processo figlio iniziato\n");
	pid =getpid();
	while(1){
		printf("Il processo figlio %d lavora\n",pid );
		sleep(1);
	}
}
