/*
Scrivere un programma C che prenda come parametro un file
inputFile (controllare che il numero di argomenti passati sia
corretto). Il programma dovrà aprire tale file, creare un processo
figlio, PF che scriverà nel file inputFile una stringa inserita da
tastiera. Il padre attenderà il figlio e visualizzerà sul video il
contenuto del file inputFile
*/
#include <stdio.h>
#include <sys/types.h>
#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdlib.h>
#include <string.h>
int main(int argc, char const *argv[])
{

	if(argc<2){
		printf("errore \n");
		exit(1);
	}
	char *buf= malloc(200*sizeof(char));
	pid_t PF;
	int fd;
	unlink(argv[1]);
	fd=open(argv[1],O_RDWR|O_CREAT,S_IRWXU|S_IRWXG|S_IRWXO);

	if((PF=fork())==0){
		char contenuto[500];
		printf("Immettere la stringa\n");
		fgets(contenuto,200,stdin);
		write(fd, contenuto, strlen(contenuto));
		exit(0);
	}
	waitpid(PF,NULL,0);
	lseek(fd,0L, SEEK_SET);

	do{
		printf("%s",buf );
	}while(read(fd, buf,sizeof(buf))>0);
	close(fd);
	return 0;
}