#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
const char *nomeFile = NULL;
size_t n =1;
int main(int argc, char const *argv[])
{
	if (argc < 2){
		printf("Errore input\n" );
	}else{
		nomeFile = argv[1];
		int fd;
		unlink(nomeFile);
		fd = open(nomeFile,O_RDWR|O_CREAT,777);
		pid_t PF;
		PF=fork();
		if (PF==0)
		{
			printf("Inserisci la stringa che vuoi scrivere\n");
			char *name = malloc (256);
			fgets (name, 256, stdin);
			write(fd,name,strlen(name));
			close(fd);
		}
		else if(PF>0){
			wait(&PF);
			fd = open(nomeFile,O_RDONLY);

			char *buf= malloc (sizeof(1024));
			do{
				printf("%s\n",buf );
			}while(read(fd,buf,sizeof(buf))>0); 
			close(fd);
			exit
		}
	}
	return 0;
}