#include <stdio.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	pid_t pid;
	pid = fork();
	if (pid==0)
	{
		printf("Sono il figlio con (PID: %d)\n", getpid());
	}
	else if (pid>0)
	{
		printf("Sono il padre con (PID: %d)\n", getpid());

	}
	return 0;
}