#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char const *argv[])
{
	if (argc<2)
	{
		printf("errore\n");
	}
	else{
		int num = atoi(argv[1]);
		pid_t pid;
		pid = fork();
		if (pid==0)
		{
			printf("Sono il figlio %d\n", num+15);
		}
		else if(pid>0){
			printf("Sono il padre %d\n", num+10);

		}

	}
	return 0;
}